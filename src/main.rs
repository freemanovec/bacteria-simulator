use orbtk::{prelude::*, render::platform::RenderContext2D, shell::ShellRequest};
use std::thread;
use std::time::Duration;
use noise::{NoiseFn, Perlin, Seedable};
use rand::prelude::*;

use std::{
    vec::Vec,
    iter::repeat,
    sync::{
        Arc,
        Mutex,
        MutexGuard
    }
};
use core::fmt::{
    Debug,
    Formatter,
    Result as FormattingResult
};


// game mechanics

#[derive(Clone)]
struct SimulationCell {
    pub nutrition: f32,
    pub alive: bool,
    pub eligible_for_reproduction: bool
}

#[derive(Default, Clone)]
pub struct SimulationState {
    map_cells: Arc<Mutex<Vec<SimulationCell>>>,
    map_width: usize,
    map_height: usize
}

impl SimulationState {

    pub fn new(map_width: usize, map_height: usize) -> Self {
        let length = map_width * map_height;
        let mut map_cells: Vec<SimulationCell> = Vec::with_capacity(length);
        let default_cell = SimulationCell {
            nutrition: 0.,
            alive: false,
            eligible_for_reproduction: false
        };
        let default_cell_generator = repeat(default_cell);
        map_cells.extend(default_cell_generator.take(length).collect::<Vec<SimulationCell>>());
        SimulationState {
            map_cells: Arc::new(Mutex::new(map_cells)),
            map_width: map_width,
            map_height: map_height
        }
    }

    pub fn step(&self) {
        let cells = self.map_cells.clone();
        let cells = cells.lock();
        let mut cells = cells.expect("Unable to lock simulation cells");
        let data_length = self.map_width * self.map_height;
        for i in 0..data_length {
            if cells[i].alive {
                if cells[i].nutrition > 0. && cells[i].eligible_for_reproduction {
                    // calculate reproduction success
                    let reproduce = random::<f32>() < 0.3;
                    if reproduce {
                        // spread to neighbors
                        if i > 0 {
                            cells[i - 1].alive = true; // left
                        }
                        if i < data_length - 1 {
                            cells[i + 1].alive = true; // right
                        }
                        if i as isize - self.map_width as isize >= 0 {
                            cells[i - self.map_width].alive = true; // top
                        }
                        if i + self.map_width < data_length {
                            cells[i + self.map_width].alive = true; // bottom
                        }
                    }
                    cells[i].nutrition -= 0.1; // unsuccessful reproduction also consume nutritions
                    if cells[i].nutrition < 0. {
                        cells[i].nutrition = 0.;
                    }
                } else if !cells[i].eligible_for_reproduction {
                    cells[i].eligible_for_reproduction = true;
                } else {
                    // die
                    cells[i].alive = false;
                }
            }
        }
    }

    pub fn init(&self) {
        let cells = self.map_cells.clone();
        let cells = cells.lock();
        let mut cells = cells.expect("Unable to lock simulation cells");

        println!("Initializing nutrition information");
        let data_length = self.map_width * self.map_height;
        let center_ix = self.map_height / 2 * self.map_width + self.map_width / 2;
        let mut perlin_source = Perlin::new();
        let mut random_source = thread_rng();
        while cells[center_ix].nutrition < 0.7 {
            let seed = random_source.next_u32();
            perlin_source = perlin_source.set_seed(seed);
            println!("Generating map with seed {}", perlin_source.seed());
            for i in 0..data_length {
                let x = i % self.map_width;
                let y = i / self.map_height;
                let x = x as f64 / 100.;
                let y = y as f64 / 100.;
                let nutrition_value = (perlin_source.get([x, y]) + 1.) / 2.;
                let nutrition_value = nutrition_value as f32;
                cells[i].nutrition = nutrition_value;
            }
        }

        cells[center_ix].alive = true;
    }

    pub fn get_live_count(&self) -> usize {
        let cells = self.map_cells.clone();
        let cells = cells.lock();
        let cells = cells.expect("Unable to lock simulation cells");
        cells.iter().map(|el| if el.alive { 1 } else { 0 }).sum()
    }

}

impl Debug for SimulationState {
    fn fmt(&self, f: &mut Formatter<'_>) -> FormattingResult {
        let live_count: usize = self.get_live_count();
        write!(f, "Simulation with {} live cells", live_count)
    }
}

impl PartialEq for SimulationState {
    fn eq(&self, other: &SimulationState) -> bool {
        self.get_live_count() == other.get_live_count()
    }
}

// rendering mechanics

#[derive(Clone, PartialEq, Pipeline)]
struct Graphic2DPipeline {
    simulation_state: SimulationState
}

impl Graphic2DPipeline {
    fn new(simulation_state: SimulationState) -> Self {
        simulation_state.init();
        Graphic2DPipeline {
            simulation_state: simulation_state
        }
    }

    fn build_color(&self, red: u8, green: u8, blue: u8) -> u32 {
        255 << 24 | (red as u32) << 16 | (green as u32) << 8 | (blue as u32)
    }

    fn initialize(&self, mut data: Vec<u32>, len: usize, color: (u8, u8, u8)) -> Vec<u32> {
        let color = self.build_color(color.0, color.1, color.2);
        data.extend(repeat(color).take(len).collect::<Vec<u32>>());
        data
    }

    fn fill(&self, data: &mut [u32], color: (u8, u8, u8)) {
        let color = self.build_color(color.0, color.1, color.2);
        let len = data.len();
        for i in 0..len {
            data[i] = color;
        }
    }

    fn set_pixel(&self, data: &mut [u32], x: usize, y: usize, color: (u8, u8, u8)) {
        let index = y * self.simulation_state.map_width + x;
        self.set_pixel_on_index(data, index, color);
    }

    fn set_pixel_on_index(&self, data: &mut [u32], index: usize, color: (u8, u8, u8)) {
        let color = self.build_color(color.0, color.1, color.2);
        data[index] = color;
    }
}

impl render::RenderPipeline for Graphic2DPipeline {
    fn draw(&self, render_target: &mut render::RenderTarget) {

        self.simulation_state.step();
        
        let image_data_length = self.simulation_state.map_width * self.simulation_state.map_height;
        let image_data: Vec<u32> = Vec::with_capacity(image_data_length);
        let mut image_data = self.initialize(image_data, image_data_length, (100, 100, 100));
        let image_data = image_data.as_mut_slice();

        let cells = self.simulation_state.map_cells.clone();
        let cells = cells.lock();
        let cells = cells.expect("Unable to lock simulation cells");

        let color_alive = (255, 20, 147);
        for (pos, cell) in cells.iter().enumerate() {
            if cell.alive {
                self.set_pixel_on_index(image_data, pos, color_alive);
            } else {
                let green = (80. * cell.nutrition) as u8;
                self.set_pixel_on_index(image_data, pos, (0, green, 0));
            }
        }

        render_target.draw(image_data);
    }
}

#[derive(Default, AsAny)]
struct MainState {
    thr: Option<thread::JoinHandle<()>>
}

impl State for MainState {
    fn init(&mut self, _: &mut Registry, ctx: &mut Context) {
        let sender = ctx.request_sender();

        self.thr = Some(thread::spawn(move || {
            loop {
                sender.send(ShellRequest::Update).unwrap();
                thread::sleep(Duration::from_millis(5));
            }
        }));
    }
}

widget!(MainView<MainState> {
    simulation_width: usize,
    simulation_height: usize
});

fn get_usize_from_prop(opt: &std::option::Option<PropertySource<usize>>, default_val: usize) -> usize {
    match opt {
        Some(opt) => {
            match opt {
                &PropertySource::Value(internal_width) => {
                    internal_width
                },
                _ => default_val
            }
        },
        _ => default_val
    }
}

impl Template for MainView {
    fn template(self, _: Entity, ctx: &mut BuildContext) -> Self {
        let width = get_usize_from_prop(&self.simulation_width, 200);
        let height = get_usize_from_prop(&self.simulation_height, 200);

        self.name("MainView").child(
            Canvas::create()
                .attach(Grid::row(1))
                .render_pipeline(RenderPipeline(Box::new(Graphic2DPipeline::new(
                    SimulationState::new(width, height)
                ))))
                .build(ctx)
        )
    }
}

fn main() {
    let simulation_map_width = 1280;
    let simulation_map_height = 720;

    Application::new()
        .window(move |ctx| {
            Window::create()
                .title("Bacteria Simulator")
                .size(simulation_map_width as f64, simulation_map_height as f64)
                .child(
                    MainView::create()
                    .simulation_width(simulation_map_width)
                    .simulation_height(simulation_map_height)
                    .build(ctx)
                )
                .build(ctx)
        })
        .run();
}
